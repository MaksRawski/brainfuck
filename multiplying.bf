cell 0 has first multiplier
cell 1 has second multiplier
cell 2 is a tmp cell
cell 3 is result

++++++ first multiplier 
>
+++++ second multiplier
<
[- run this whole loop *cell 0* number of times
	>[->+>+<<] move cell 1 to tmp and result
	>[-<+>] move tmp to cell 1
	<< go back to cell 0
]
>>>. print the result
