squares
print squares of numbers
uses multiplying algorithm

>
+ number to square in cell 1

<++++++++++++ init the loop (how many integers to square)
[
  >[->+>+<<]>>[-<<+>>]<< copy number to cells 1 and 2 through tmp

 [- run this whole loop *cell 1* number of times
  >[->+>+<<] move cell 2 to tmp and result
  >[-<+>] move tmp to cell 2
  << go back to cell 1
 ]
 >[-<+>] move cell 2 to cell 1

 >>. print the result(cell 4)
 [-] zero out cell 4
 <<<+
 <- run this whole loop the init value of times
]
