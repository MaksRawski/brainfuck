[ "$1" == "" ] && (echo "No input file provided."; exit 1)
grep -o -P '[\,\.\+\-\[\]\<\>]*' "$1" | tr -d '\n'
