import os
import sys
import argparse

parser = argparse.ArgumentParser(description='Interpret a brainfuck file.')
parser.add_argument("file", nargs='?', type=str, help="brainfuck input file")
parser.add_argument("-n","--number-mode", action="store_true", help="instead of printing characters, print numbers and don't wrap at 256")
args = parser.parse_args()

if args.file == None:
    print("No input file provided. Exiting.")
    exit(0)
else:
    try:
        fl = open(args.file, "r")
    except FileNotFoundError:
        print(f"File '{args.file}' not found.")
        exit(0)
    except PermissionError:
        print(f"Cannot access file '{args.file}'.")
        exit(0)

tape = [0] * 40000
currentCell = 0
programCounter = 0
program = fl.read()
loops = []

# helper function
def matchingBracketIndex(index):
    brackets = []
    while index < len(program) - 1:
        if program[index] == "[":
            brackets.append(index)
        elif program[index] == "]":
            if len(brackets) == 1:
                return index
            else:
                brackets.pop()
        index += 1
    print("No matching closing bracket found!")
    exit(1)

# chars' definitions
def dot():
    if args.number_mode:
        print(tape[currentCell])
    else:
        print(chr(tape[currentCell]), end="")

def comma():
    userInput = ord(sys.stdin.read(1))
    if userInput > 255:
        print("Character given is not in range.")
        exit(1)

    tape[currentCell] = userInput
def plus():
    if tape[currentCell] == 255 and (not args.number_mode):
        # wrap
        tape[currentCell] = 0
    else:
        tape[currentCell] += 1

def minus():
    if tape[currentCell] == 0 and (not args.number_mode):
        # wrap
        tape[currentCell] = 255
    else:
        tape[currentCell] -= 1

def enterLoop():
    global programCounter
    if tape[currentCell] == 0:
        # skip the whole loop
        programCounter = matchingBracketIndex(programCounter) + 1
    else:
        loops.append(programCounter+1)

def closeLoop():
    global programCounter
    if tape[currentCell] == 0:
        loops.pop()
        programCounter += 1
    else:
        programCounter = loops[len(loops) - 1]

def moveLeft():
    global currentCell
    if currentCell == 0:
        print("Program tried to go to cell -1.")
        print(f"Error occured at character number: {programCounter}")
        exit(1)
    else:
        currentCell -= 1

def moveRight():
    global currentCell
    if currentCell == len(tape)-1:
        print(f"Program tried to go to cell {len(tape)} but the last cell has index {len(tape)-1}.")
        print(f"Error occured at character number: {programCounter}")
        exit(1)
    currentCell += 1

chars = {
    ".": dot,
    ",": comma,
    "+": plus,
    "-": minus,
    "[": enterLoop,
    "]": closeLoop,
    "<": moveLeft,
    ">": moveRight
}

while programCounter < len(program) - 1:
    char = program[programCounter]

    # ignore all characters that aren't supported by brainfuck
    if char in chars.keys():
        chars[char]()

    if char != "]":
        programCounter += 1
