fibonacci

a = cell 0
b = cell 1
c = cell 2
tmp = cell 3
iterator = cell 4

>>+  put 1 in c

>>+++++++++++++++ how many times to run the algorithm
[
 print a
 <<<<.

 >[-] zero out b

 < go to a
 [->+<] b = a

 >> go to c
 [-<<+>>] a = c

c = a + b is:
 [-] zero out c

 << go to a
 [->>+>+<<<] move a to c and tmp

 >>> go to tmp
 [-<<<+>>>] move tmp back to a

 << go to b
 [->+>+<<] move b to c and tmp

 >> go to tmp
 [-<<+>>] move tmp back to b
end

 >- decrement the iterator 
]
